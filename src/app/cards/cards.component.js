(function() {
  'use strict';

  angular
    .module('angularAssignment')
    .component('cards', {
      templateUrl: 'app/cards/cards.html',
      controller: CardsController,
      bindings: {
        contacts: '='
      }
    });

  /** @ngInject */
  function CardsController($scope) {
    var ctrl =  this;

    ctrl.init = function() {
      ctrl.contacts.map(function(contact) {
        contact.checkBoolean = false;
        return contact;
      });
    }
    // ADD CHECK PROPERTY
      ctrl.init();
  // LAUCH NG-CLICK
    $scope.showInfo = function(index) {

      
      ctrl.contacts[index].checkBoolean =! ctrl.contacts[index].checkBoolean;

    }
    // TOGGLE NG-CLASS
    $scope.slideActive = function(index) {
      return ctrl.contacts[index].checkBoolean;
    }

  }
})();
